/*
    Fix image resizing on mobile browsers caused by the url bar
*/
(function() {
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        document.getElementById('home').style.height = screen.height * 0.6 + 'px'
    }
})()