const gulp = require('gulp')
const cleanCss = require('gulp-clean-css')
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')

gulp.task('css', () => {
    gulp
        .src('main.css')
        .pipe(concat('main.min.css'))
        .pipe(autoprefixer())
        .pipe(cleanCss())
        .pipe(gulp.dest('.'))
})

gulp.task('js', () => {
    gulp
        .src('main.js')
        .pipe(concat('main.min.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('.'))
})

gulp.task('default', ['css', 'js'])