$(() => {
    const scrollSpyArray = $('.c-section').toArray().map((domElement) => {
        const el = $(domElement)

        return {
            start: el.position().top - el.height()/2,
            end: el.position().top + el.height()/2,
            enter: () => $('.c-nav__right__menu__item__link[href="#' + el.attr('id') + '"]').addClass('c-nav__right__menu__item__link--active'),
            leave: () => $('.c-nav__right__menu__item__link[href="#' + el.attr('id') + '"]').removeClass('c-nav__right__menu__item__link--active')
        }
    })

    $(window).scroll(() => {
        let offset = document.body.scrollTop || document.documentElement.scrollTop

        scrollSpyArray.forEach((element) => element[(element.start < offset && element.end > offset) ? "enter" : "leave"]())
    })

    $('.c-nav__right__menu__item__link').click(function(event) {
        event.stopPropagation()

        $('body').animate({
            scrollTop: $($(this).attr('href')).position().top
        }, 'slow')
    })

    $('#menu_toggle').click(() => $('.c-nav__right__menu').toggleClass('c-nav__right__menu--hidden'))
})